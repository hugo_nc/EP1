#include <iostream>
#include <fstream>
#include "decifraPGM.hpp"

using namespace std;

ifstream arquivoImagemDecifraPGM;

//Métodos

DecifraPGM::DecifraPGM(char caminhoDaImagem[100]){
  arquivoImagemDecifraPGM.open(caminhoDaImagem,fstream::in);

}

char DecifraPGM::decifraRespostaPGM(int cont ,int inicio){

  char byteArquivo;
  int bitExtraido;
  int n=posicao+inicio+(cont*8);
  arquivoImagemDecifraPGM.seekg(n,ios_base::beg);

    i=0;
    caracterExtraido = 0x00;

    while(i<8){
      arquivoImagemDecifraPGM.get(byteArquivo);
      bitExtraido=(byteArquivo & 0x01);
      caracterExtraido = (caracterExtraido | (bitExtraido));
      //condição para efetuar o switch do bit apenas até a penúltima posição
      if(i<7){
        caracterExtraido = caracterExtraido << 1;
      }
      else{
        break;
      }
      i++;
    }

    return caracterExtraido;
}
