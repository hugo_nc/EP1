#include <iostream>
#include <cstring>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "imagem.hpp"

using namespace std;
//Declarando variável do tipo ifstream para abrir a imagem
ifstream arquivoImagem;

//Chamando o construtor padrão
Imagem::Imagem(){
}

//Chamando o construtor sobrecarregado
Imagem::Imagem(char caminhoDaImagem[100]){
  //O contador de caracteres serve para saber em que posição termina a leitura dos dados do cabeçalho
   contadorDeCaracteres=0;
   arquivoImagem.open(caminhoDaImagem,fstream::in);

   if(!arquivoImagem.is_open()){
      system("clear");
      cout<<"Arquivo não localizado, verifique se informou corretamente."<<endl;
      cout<<"Terminando a aplicação"<<endl;
      exit(0);
   }
}

//Chamando os métodos acessores as informações do arquivo

//Coleta o número mágico para definir qual tipo de imagem e auxiliar futura tomanda de decisões
char Imagem::getNumeroMagico(){
  contador=0;

  do{
    arquivoImagem.get(informacao[contador]);
    numeroMagico[contador]=informacao[contador];
    return numeroMagico[contador];

    contador++;
  }while(informacao[contador-1]!='\n');
  contadorDeCaracteres++;
  contadorDeCaracteres+=contador;
}

//Método para capturar a posição inicial para imagens PGM no meio de comentários no cabeçalho de imagens
int Imagem::getPosicao(){
  contador=0;
  string informacaoTemporario;

  do{
    arquivoImagem.get(informacao[contador]);

      if(informacao[contador]=='#'){
        aux=contador;
        arquivoImagem.seekg(aux,ios_base::cur);

        do{
          informacao[contador]=arquivoImagem.get();
          informacaoTemporario+=informacao[contador];
          contador++;
        }while(informacao[contador-1]!=' ');
          contadorDeCaracteres++;
          break;
      }
      else{
        contador++;
      }
  }while(1);

  contadorDeCaracteres++;
  contadorDeCaracteres+=contador;

  //Função do C++ para converter string em int e assim facilitar a manipulação dos dados
  posicao = stoi (informacaoTemporario);
  return posicao;
}

//Método que pega a largura da imagem contida no cabeçalho
int Imagem::getLargura(){
  contador=0;
  string informacaoTemporario;
  char auxiliar;

  arquivoImagem.seekg(contador,ios_base::cur);

  do{
    arquivoImagem.get(informacao[contador]);
    auxiliar=arquivoImagem.peek();

    //Função do C++ que verifica se o determinado caracter é um valor numérico
    if(isdigit(informacao[contador])){
      if(isdigit(auxiliar)){
        informacaoTemporario+=informacao[contador];
        contador++;
      }
      else{
        informacaoTemporario+=informacao[contador];
        contador++;
        break;
      }
    }
    else{
      contador++;
    }
  }while(1);
    contadorDeCaracteres++;
    contadorDeCaracteres+=contador;

  //Função do C++ para converter string em int e assim facilitar a manipulação dos dados
  largura = stoi (informacaoTemporario);
  return largura;
}

//Método para coletar a altura da imagem
int Imagem::getAltura(){
  contador=0;
  string informacaoTemporario;
  char auxiliar;

  arquivoImagem.seekg(contador,ios_base::cur);

  do{
    arquivoImagem.get(informacao[contador]);
    auxiliar=arquivoImagem.peek();

    if(isdigit(informacao[contador])){
      if(isdigit(auxiliar)){
        informacaoTemporario+=informacao[contador];
        contadorDeCaracteres++;
      }
      else{
        informacaoTemporario+=informacao[contador];
        contadorDeCaracteres++;
        break;
      }
    }
    else{
      contador++;
    }
  }while(1);

    contadorDeCaracteres++;
    contadorDeCaracteres+=contador;

    altura = stoi (informacaoTemporario);
    return altura;
}

//Método para se pegar a cor máxima do pixel
int Imagem::getCorMaxima(){

  contador=0;
  string informacaoTemporario;
  char auxiliar;

  arquivoImagem.seekg(contador,ios_base::cur);

  do{
    arquivoImagem.get(informacao[contador]);
    auxiliar=arquivoImagem.peek();

    if(isdigit(informacao[contador])){
      if(isdigit(auxiliar)){
        informacaoTemporario+=informacao[contador];
        contador++;
      }
      else{
        informacaoTemporario+=informacao[contador];
        contadorDeCaracteres++;
        break;
      }
    }
    else{
      contador++;
    }
  }while(1);

  contadorDeCaracteres++;
  contadorDeCaracteres+=contador;

  corMaxima = stoi (informacaoTemporario);
  return corMaxima;
}

//Método que retorna o valor da quantidade de  caracteres faciltando informar o ponto inicial para modificação dos dados
int Imagem::getContador (){
  return contadorDeCaracteres;
}
