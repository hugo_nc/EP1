#include <iostream>
#include <fstream>
#include <string>
#include "decifraPPM.hpp"

using namespace std;

ifstream arquivoImagemDecifraPPM;

//Métodos

//Construtor de imagem PPM
DecifraPPM::DecifraPPM(char caminhoDaImagem[100]){
  arquivoImagemDecifraPPM.open(caminhoDaImagem,fstream::in);

}

char DecifraPPM::decifraRespostaPPM(int inicio,int filtro,string nomeArquivoSaidaPPM){
  int i,j,k;
  char c='0';
  ofstream arquivoRespostaPPM;
  arquivoRespostaPPM.open(nomeArquivoSaidaPPM);
  arquivoImagemDecifraPPM.seekg(inicio,ios_base::beg);

  //Especificando o cabeçalho, pois sem este a imagem fica corrompida
  //Valor dado de P6, pois a aplicação apenas cuida de dois formatos
  arquivoRespostaPPM<<"P6\n";
  //Demais valores adivindos do set da classe mãe
  arquivoRespostaPPM<<largura<<" ";
  arquivoRespostaPPM<<altura<<"\n";
  arquivoRespostaPPM<<corMaxima<<"\n";

//Laço dos filtros

  for(i=0;i<largura*3;i++){
    for(j=0;j<altura;j++){
      for(k=0;k<3;k++){
        //Filtro RED
        if(filtro==1){
          switch (k){
            case 0:
            arquivoImagemDecifraPPM.get(resposta);
            arquivoRespostaPPM<<resposta;
            break;
            default :
            arquivoImagemDecifraPPM.get(resposta);
            resposta=0x0;
            arquivoRespostaPPM<<resposta;
            break;
          }
        }

        //Filtro GREEN
        if(filtro==2){
          switch (k){
            case 1:
            arquivoImagemDecifraPPM.get(resposta);
            arquivoRespostaPPM<<resposta;
            break;
            default :
            arquivoImagemDecifraPPM.get(resposta);
            resposta=0x0;
            arquivoRespostaPPM<<resposta;
            break;
          }
        }

        //Filtro BLUE
        if(filtro==3){
          switch (k){
            case 2:
            arquivoImagemDecifraPPM.get(resposta);
            arquivoRespostaPPM<<resposta;
            break;
            default :
            arquivoImagemDecifraPPM.get(resposta);
            resposta=0x0;
            arquivoRespostaPPM<<resposta;
            break;
          }
        }

      }
    }
  }
  return c;
}
