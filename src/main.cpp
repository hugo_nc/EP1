#include <iostream>
#include <fstream>
#include <cstring>
#include <unistd.h>

//incluindo os headers criados e necessários
#include "imagem.hpp"
#include "decifraPGM.hpp"
#include "decifraPPM.hpp"

using namespace std;

int main(){

  int opcao;

  for(int x=0;x<3;x++){
    for(int y=0;y<50;y++){
      if(x==0){
        cout<<"#";
      }
      if(x==1){
        cout<<"#    Exercício de Programação 01 - Bem Vindo!    #";
        break;
      }
      if(x==2){
        cout<<"#";
      }
    }
    cout<<endl;
  }

  cout<<"1- Abrir imagem"<< endl;
  cout<<"2- Sair!"<<endl;
  cout<<"Digite a opção desejada: ";
  cin>>opcao;
  Imagem * imagem_1;

  while(1){

  if(opcao==1){
  //Declaração de variáveis úteis para o código
  char caminhoImagem[100];
  char numeroMagico[10]="";
  int posicao,largura,altura,corMaxima,inicio,filtro;

  //Captura do caminho da imagem
  cout<<"Digite o caminho da imagem (Ex.: img/lena.pgm): ";
  cin>>caminhoImagem;

  //Criação do objeto Imagem

  imagem_1=new Imagem(caminhoImagem);

//Comparação para saber o tipo de imagem e assim gerar objetos métodos a se seguirem
  for(int i=0;i<2;i++){
    numeroMagico[i]=imagem_1->getNumeroMagico();

  }

  //Condição para ser PGM
  if(!strcmp(numeroMagico,"P5")){

    //Criação do objeto para imagens PGM
    DecifraPGM * decifraPGM;
    decifraPGM = new DecifraPGM(caminhoImagem);

    //Passando valores do arquivo imagem para variáveis
    posicao=imagem_1->getPosicao();
    largura=imagem_1->getLargura();
    altura=imagem_1->getAltura();
    corMaxima=imagem_1->getCorMaxima();
    inicio=imagem_1->getContador();

    //Setando os valores contidos nas variáveis para os métodos de decifragem
    decifraPGM->setPosicao(posicao);
    decifraPGM->setLargura(largura);
    decifraPGM->setAltura(altura);
    decifraPGM->setCorMaxima(corMaxima);

    //Criando arquivo de saída
    string nomeArquivoSaidaPGM;
    char const* nomeTemp;
    //Pede nome para o arquivo resposta
    cout<<"Digite o nome que deseja para arquivo saída (Ex.: img/resposta.txt): ";
    cin>>nomeArquivoSaidaPGM;

    nomeTemp=nomeArquivoSaidaPGM.c_str();
    ofstream arquivoResposta;

  while(1){
  if( access(nomeTemp, F_OK ) != -1 ){
      cout<<"Arquivo já existente deseja sobrescrever (S = sim// outros = não): ";
      char r;
      cin>>r;
      if(r=='S'){
        arquivoResposta.open(nomeArquivoSaidaPGM);
        break;
      }
      else{
        cout<<"Digite o nome que deseja para arquivo saída (Ex.: img/resposta.txt): ";
        cin>>nomeArquivoSaidaPGM;
        nomeTemp=nomeArquivoSaidaPGM.c_str();
      }
    }
    else{
      arquivoResposta.open(nomeArquivoSaidaPGM);
      break;
    }
  }
    char resposta;
    int i=0;
    //Executa o método de decifrar imagens PGM e guarda resultados no arquivo criado
    resposta=decifraPGM->decifraRespostaPGM(i,inicio);
    arquivoResposta<<resposta;
    i++;
    do{
      resposta=decifraPGM->decifraRespostaPGM(i,inicio);
      arquivoResposta<<resposta;
      i++;
    }while(resposta!='#');
    system("clear");
    cout<<"O conteúdo escondido na imagem se encontra no arquivo "<<nomeArquivoSaidaPGM<<"."<<endl;
    cout<<"Aplicação finalizada"<<endl;
    arquivoResposta.close();
    //deleta o objeto após ter feito uso do mesmo

    delete(decifraPGM);
    break;
}

//Condição para ser imagem PPM
  else{
    if(!strcmp(numeroMagico,"P6")){

      //Instância o objeto
      DecifraPPM * decifraPPM;
      decifraPPM = new DecifraPPM(caminhoImagem);

      //Coleta o filtro a ser aplicado
      cout<<"Selecione um dos filtros disponíveis:"<<endl;
      cout<<"1 - Filtro RED - R"<<endl;
      cout<<"2 - Filtro GREEN - G"<<endl;
      cout<<"3 - Filtro BLUE - B"<<endl;
      cout<<"Digite o valor de um filtro: ";
      cin>>filtro;

      while(1){
        if((filtro==1) || (filtro==2) || (filtro==3)){
          break;
        }
        else{
          cout<<"Filtro não cadastrado, informe um filtro listado: ";
          cin>>filtro;
        }
      }


      //Coleta os valores da imagem
      largura=imagem_1->getLargura();
      altura=imagem_1->getAltura();
      corMaxima=imagem_1->getCorMaxima();
      inicio=imagem_1->getContador();

      //Passa os valores da imagem para os métodos que decifram PPM
      decifraPPM->setLargura(largura);
      decifraPPM->setAltura(altura);
      decifraPPM->setCorMaxima(corMaxima);

      //Pede nome para o arquivo resposta
      string nomeArquivoSaidaPPM;
      char const*   nomeTemp;
      nomeTemp=nomeArquivoSaidaPPM.c_str();

      cout<<"Digite o nome que deseja para arquivo saída (Ex.: img/resposta.ppm): ";
      cin>>nomeArquivoSaidaPPM;
      nomeTemp=nomeArquivoSaidaPPM.c_str();

      while(1){
      if( access(nomeTemp, F_OK ) != -1 ){
          cout<<"Arquivo já existente deseja sobrescrever (S = sim// outros = não): ";
          char r;
          cin>>r;
          if(r=='S'){
            break;
          }
          else{
            cout<<"Digite o nome que deseja para arquivo saída (Ex.: img/resposta.ppm): ";
            cin>>nomeArquivoSaidaPPM;
            nomeTemp=nomeArquivoSaidaPPM.c_str();
          }
        }
        else{
          break;
        }
      }

      //Executa o processo de decifrar PPM
      char sucesso;
      sucesso=decifraPPM->decifraRespostaPPM(inicio,filtro,nomeArquivoSaidaPPM);
      if(sucesso=='0'){
        system("clear");
        cout<<"O conteúdo escondido na imagem se encontra no arquivo "<< nomeArquivoSaidaPPM<<"."<<endl;
        cout<<"Aplicação finalizada"<<endl;
        break;
      }
      else{
        cout<<"Ocorreu um erro no momento de decifrar"<<endl;
        break;
      }

      //Deleta objeto
      delete(decifraPPM);

  }

//Condição de arquivo não suportado
  else{
    cout<<"Formato não suportado pela aplicação."<<endl;
    break;
  }
}

  //Deleta o objeto
  }
  else{
    if(opcao==2){
      system("clear");
      cout<<"Terminando a aplicação"<<endl;
      exit(0);
      delete(imagem_1);
    }
    else{
      cout<<"Opção não Cadastrada, por favor tente novamente:"<<endl;
      cout<<"1- Abrir imagem"<< endl;
      cout<<"2- Sair!"<< endl;
      cout<<"Digite a opção desejada: ";
      cin>>opcao;

    }
  }
}

}
