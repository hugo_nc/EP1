#include <iostream>
#include "decifra.hpp"

using namespace std;

//Construtor padrão,apenas por definição, pois está é uma classe abstrata
Decifra::Decifra(){
  }

//Métodos para setar valores nas variáveis usadas nas classes filhas
void Decifra::setPosicao(int posicao){
  this->posicao=posicao;
}

void Decifra::setLargura(int largura){
  this->largura=largura;
}

void Decifra::setAltura(int altura){
  this->altura=altura;
}

void Decifra::setCorMaxima(int corMaxima){
  this->corMaxima=corMaxima;
}
