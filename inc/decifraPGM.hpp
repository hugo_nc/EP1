#ifndef DECIFRAPGM_HPP
#define DECIFRAPGM_HPP

#include "decifra.hpp"

//A classe DecifraPGM é  classe filha de Decifra
class DecifraPGM : public Decifra{
public:
  //Método construtor
  DecifraPGM(char caminhoDaImagem[100]);
  //Gera a resposta para imagens em PGM
  char decifraRespostaPGM(int cont,int inicio);

};
#endif
