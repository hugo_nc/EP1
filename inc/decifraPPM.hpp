#ifndef DECIFRAPPM_HPP
#define DECIFRAPPM_HPP

#include "decifra.hpp"
#include <string>

//A classe DecifraPPM é  classe filha de Decifra
class DecifraPPM : public Decifra{
public:
  //Método construtor
  DecifraPPM(char caminhoDaImagem[100]);
  //Método que retorna a respota para imagens em PPM
  char decifraRespostaPPM(int cont,int inicio,std::string nomeArquivoSaidaPPM);

};
#endif
