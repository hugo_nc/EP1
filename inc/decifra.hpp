#ifndef DECIFRA_HPP
#define DECIFRA_HPP

class Decifra{
  //Atributos
protected:
  //Variáveis auxiliares para o código
  char caminhoDaImagem[100];
  int i;
  //Variáveis para armazenamento
  int posicao;
  int largura;
  int altura;
  int corMaxima;
  char caracterExtraido;
  char resposta;

 //Métodos
public:
  //Construtor padrão
  Decifra();
  //Acessores para coletar os dados do arquivo imagem
  void setPosicao(int posicao);
  void setLargura(int largura);
  void setAltura(int altura);
  void setCorMaxima(int corMaxima);

};
#endif
