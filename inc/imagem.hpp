#ifndef IMAGEM_HPP
#define IMAGEM_HPP

class Imagem{
//Atributos

public:
  //Variáveis auxiliares de uso global
  int contador;
  int contadorTemporario;
  int aux;
  int contadorDeCaracteres;

protected:
  //Variáveis auxiliares para o código
  char caminhoDaImagem[100];
  char informacao[];
  char tamanho[6];
  int i;
  char caracter;
  char caracterExtraido;

  //Variáveis para armazenamento dos dados advindos da imagem
  char numeroMagico[2];
  int posicao;
  int largura;
  int altura;
  int corMaxima;
  int inicio;

 //Métodos
public:
  //Construtor padrão
  Imagem();
  //Construtor sobrecarregado
  Imagem(char caminhoDaImagem[100]);

  //Acessores para coleta dos dados do arquivo
    char getNumeroMagico();
    int getPosicao();
    int getLargura();
    int getAltura();
    int getCorMaxima();
    int getContador();

};
#endif
