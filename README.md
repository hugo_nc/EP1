# **O projeto**
Este software é voltado para solucionar problemas envolvendo esteganografia em imagens do formato **.ppm** e **.pgm** utilizando C++.

# **Como instalar**
### **Compilando**
Com a cópia do repositório em sua máquina, execute o comando abaixo para _**compilar**_ :
``` 
//O comando abaixo deve ser realizado no terminal dentro da pasta raiz do projeto
make
``` 

### **Executando**
Para _**executar**_ o programa já compilado:
``` 
//O comando abaixo deve ser realizado no terminal dentro da pasta raiz do projeto
make run
``` 
Feito isso, apenas siga as instruções no próprio software para continuar a utilizá-lo.

**OBS.:**   * _Para executar o programa novamente, realize o passo "Executando"._  
            * _Algumas imagens estão disponíveis para teste da aplicação no diretório "img/"_.

### **FAQ**
* Esta aplicação tem compatibilidade para quais Sistemas Operacionais?  
    O software apenas foi testado em ambiente Linux. Sendo assim, o seu desempenho em outros SO's é desconhecido.   
    É livre o teste em outros Sistemas Operacionais.
* Esta aplicação funciona em IDEs para C++?  
    O software foi desenvolvido e testado para ser executado em linha de comando no terminal.  
    É livre o teste em ambientes de desenvolvimento.
* É necessário importar alguma biblioteca para execução?  
    Não. Apenas as definidas por padrão no compilador de C++ de sua preferência.   
    O mesmo pode ser instalado pela linha de comando: 
``` 
//Execute o comando abaixo no terminal como super usuário ou utilizando sudo
apt-get install g++
```
* A quem reportar em caso de problemas?  
    Em caso de falhas, favor reportar ao seguinte e-mail: hugo_nc@outlook.com .

# **Licença**
Esta aplicação é livre,tendo seu maior enfoque no âmbito didático para disciplinas de programação Orientada a Objetos.